import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import { API_URL } from '../config'
import Layout from '../core/Layout'

const Login = () => {
  const [state, setState] = useState({ email: '', password: '', error: '', loading: false, redirectToReferrer: false })

  const { email, password, error, loading, redirectToReferrer } = state

  const handleInput = (e) => setState({ ...state, error: false, [e.target.id]: e.target.value })

  const submitInput = (e) => {
    e.preventDefault()
    const user = { email, password }
    setState({ ...state, error: false, loading: true })
    login(user)
  }

  const login = async (user) => {
    let URL, OPTIONS, headers, body, method

    method = 'POST'
    body = JSON.stringify(user)
    headers = { Accept: 'application/json', 'Content-Type': 'application/json' }

    OPTIONS = { method, headers, body }
    URL = `${API_URL}/login`

    const res = await fetch(URL, OPTIONS).then(res => res.json())
    if (res.err || res.error) return setState({ ...state, error: res.err || res.error, loading: false })

    if (typeof window !== 'undefined') localStorage.setItem('jwt', JSON.stringify(res))


    setState({ ...state, redirectToReferrer: true })
  }

  const _renderStatusMessage = () => {
    if (error) return (
      <div className="alert alert-danger" style={{ display: '' }}>
        {error}
      </div>
    )

    if (loading) return (
      <div className="alert alert-info">
        <h4>Loading...</h4>
      </div>
    )

    if (redirectToReferrer) return <Redirect to='/' />
  }

  const _renderForm = () => (
    <form action="" onSubmit={submitInput}>
      <div className="form-group">
        <label htmlFor="email" className="text-muted">Email</label>
        <input type="email" id="email" className="form-control" value={email} onChange={handleInput} />
      </div>
      <div className="form-group">
        <label htmlFor="password" className="text-muted">Password</label>
        <input type="password" id="password" className="form-control" value={password} onChange={handleInput} />
      </div>
      <button className="btn btn-primary">Submit</button>
    </form>
  )

  return (
    <Layout title='Login' description='Login to Node React Ecomm App' className='container col-md-8 offset-md-2'>
      {_renderStatusMessage()}
      {_renderForm()}
    </Layout>
  )
}
export default Login