import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { API_URL } from '../config'
import Layout from '../core/Layout'

const Register = () => {

  const [state, setState] = useState({ name: '', email: '', password: '', error: '', success: false })

  const { name, email, password, error, success } = state

  const handleInput = (e) => setState({ ...state, error: false, [e.target.id]: e.target.value })

  const submitInput = (e) => {
    e.preventDefault()
    const user = { name, email, password }
    register(user)
  }

  const register = async (user) => {
    let URL, OPTIONS, headers, body, method

    method = 'POST'
    body = JSON.stringify(user)
    headers = { Accept: 'application/json', 'Content-Type': 'application/json' }

    OPTIONS = { method, headers, body }
    URL = `${API_URL}/register`

    const res = await fetch(URL, OPTIONS).then(res => res.json())
    if (res.err !== undefined && res.err.includes('11000')) res.err = 'Email already exists!'
    if (res.err || res.error) return setState({ ...state, error: res.err || res.error })

    setState({ ...state, name: '', email: '', password: '', error: '', success: true })
  }

  const _renderStatusMessage = () => {
    if (error) return (
      <div className="alert alert-danger" style={{ display: '' }}>
        {error}
      </div>
    )

    if (!error && success) return (
      <div className="alert alert-info" style={{ display: '' }}>
        New user has been added. Please <Link to='/login'>login</Link>
      </div>
    )
  }

  const _renderForm = () => (
    <form action="" onSubmit={submitInput}>
      <div className="form-group">
        <label htmlFor="name" className="text-muted">Name</label>
        <input type="text" id="name" className="form-control" value={name} onChange={handleInput} />
      </div>
      <div className="form-group">
        <label htmlFor="email" className="text-muted">Email</label>
        <input type="email" id="email" className="form-control" value={email} onChange={handleInput} />
      </div>
      <div className="form-group">
        <label htmlFor="password" className="text-muted">Password</label>
        <input type="password" id="password" className="form-control" value={password} onChange={handleInput} />
      </div>
      <button className="btn btn-primary">Submit</button>
    </form>
  )

  return (
    <Layout title='Register' description='Register to Node React Ecomm App' className='container col-md-8 offset-md-2'>
      {_renderStatusMessage()}
      {_renderForm()}
    </Layout>
  )
}
export default Register