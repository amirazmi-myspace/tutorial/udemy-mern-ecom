import React, { useEffect, useState } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_URL } from '../config'
import { authenticated } from '../helpers/auth'

const active = (history, path) => {
  if (history.location.pathname === path) return { color: '#ff9900' }
  return { color: '#fff' }
}

const Menu = ({ history }) => {
  const [state, setState] = useState({ user: '' })

  const { user } = state

  useEffect(() => {
    if (localStorage.getItem('jwt')) return setState({ ...state, user: JSON.parse(localStorage.getItem('jwt')).user })
  }, [localStorage.getItem('jwt')])

  const logout = async () => {
    let URL, OPTIONS, method
    method = 'GET'
    if (typeof window !== 'undefined') localStorage.removeItem('jwt')

    OPTIONS = { method }
    URL = `${API_URL}/logout`

    await fetch(URL, OPTIONS).then(res => res.json())
    history.push('/')
  }

  const _renderLinks = () => {
    if (!authenticated()) return (
      <>
        <li className="nav-item ml-auto">
          <Link className="nav-link" style={active(history, '/register')} to="/register">Register</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" style={active(history, '/login')} to="/login">Login</Link>
        </li>
      </>
    )
    if (authenticated()) return (
      <>
        <li className='ml-auto mr-3' style={{ alignSelf: 'center', justifySelf: 'center', color: '#fff' }}>
          <span style={{ alignSelf: 'center', justifySelf: 'center' }}>Welcome {user.name}</span>
        </li>
        <li className="nav-item">
          <span className="nav-link" style={{ cursor: 'pointer', color: '#fff' }} onClick={logout}>Logout</span>
        </li>
      </>
    )
  }
  console.log(state)
  return (
    <div>
      <ul className="nav nav-tabs bg-primary">
        <li className="nav-item">
          <Link className="nav-link" style={active(history, '/')} to="/">Home</Link>
        </li>
        {_renderLinks()}

      </ul>
    </div>
  )
}

export default withRouter(Menu)
