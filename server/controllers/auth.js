const jwt = require('jsonwebtoken') // to generate jwt token
const expressJwt = require('express-jwt') // for auth check
const User = require('../models/user')
const { errorHandler } = require("../helpers/dbErrorHandler")

exports.register = (req, res) => {
  const user = new User(req.body)
  user.save((err, user) => {
    if (err) { return res.status(400).json({ err: errorHandler(err) }) }
    user.salt = undefined
    user.hashedPassword = undefined

    res.json({ user })
  })
}

exports.login = (req, res) => {
  // find user based on email
  const { email, password } = req.body
  User.findOne({ email }, (err, user) => {
    if (err || !user) return res.status(400).json({ err: 'User with that email does not exists. Please register!' })
    // if user is found make sure email and password match
    // create authenticate method in user model
    if (!user.authenticate(password)) return res.status(401).json({ err: 'Email and password dont match' })

    // generate a signed token with user id and secret
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET)

    // persist the token as 't' in cookie with expiry date
    res.cookie('t', token, { expire: new Date() + 9999 })

    // return response with user and token to frontend client
    const { _id, name, email, role } = user
    return res.json({ token, user: { _id, name, email, role } })
  })
}

exports.logout = (req, res) => {
  res.clearCookie('t')
  return res.json({ message: 'Logout success' })
}

exports.requireSignin = expressJwt({
  secret: process.env.JWT_SECRET,
  algorithms: ["HS256"], // added later
  userProperty: "auth",
});

exports.isAuth = (req, res, next) => {
  let user = req.profile.toString() && req.auth.toString() && req.profile._id.toString() === req.auth._id.toString()
  if (!user) return res.status(403).json({ err: 'Access Denied' })
  next()
}

exports.isAdmin = (req, res, next) => {
  if (req.profile.role === 0) return res.status(403).json({ err: 'Admin resource! Access Denied' })
  next()
}