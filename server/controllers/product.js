const fs = require('fs')
const formidable = require('formidable')
const _ = require('lodash')
const Product = require('../models/product')
const { errorHandler } = require('../helpers/dbErrorHandler')
const product = require('../models/product')

exports.productById = (req, res, next, id) => {
  Product.findById(id).exec((err, product) => {
    if (err || !product) return res.status(400).json({ err: 'Product not found' })

    req.product = product
    next()
  })
}

exports.read = (req, res) => {
  req.product.image = undefined
  return res.json(req.product)
}

exports.create = (req, res) => {
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) => {
    if (err) return res.status(400).json({ err: 'Image could not be uploaded' })

    let product = new Product(fields)

    // 1kb = 1000
    // 1mb = 1000000

    const fileSize = 1000 * 1000
    if (files.image) {
      // console.log('files.image', files.image)
      if (files.image.size > fileSize) return res.status(400).json({ err: 'Image should be below 1mb size' })

      // check for all fields
      const { name, description, price, category, quantity, shipping } = fields
      if (!name || !description || !price || !category || !quantity || !shipping) return res.status(400).json({ err: 'All fields are required' })

      product.image.data = fs.readFileSync(files.image.path)
      product.image.contentType = files.image.type
    }

    product.save((err, result) => {
      if (err) return res.status(400).json({ err: errorHandler(err) })
      res.json(result)
    })
  })
}

exports.update = (req, res) => {
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) => {
    if (err) return res.status(400).json({ err: 'Image could not be uploaded' })

    let product = req.product
    product = _.extend(product, fields)
    const fileSize = 1000 * 1000

    if (files.image) {
      if (files.image.size > fileSize) return res.status(400).json({ err: 'Image should be below 1mb size' })

      // check for all fields
      const { name, description, price, category, quantity, shipping } = fields
      if (!name || !description || !price || !category || !quantity || !shipping) return res.status(400).json({ err: 'All fields are required' })

      product.image.data = fs.readFileSync(files.image.path)
      product.image.contentType = files.image.type
    }

    product.save((err, result) => {
      if (err) return res.status(400).json({ err: errorHandler(err) })
      res.json(result)
    })
  })

}

exports.remove = (req, res) => {
  let product = req.product
  product.remove((err, deletedProduct) => {
    if (err) return res.status(400).json({ err: errorHandler(err) })

    res.json({ deletedProduct, message: 'Product deleted' })
  })
}

/* sell/arrival */
// by sell = /products?sortBy=sold&order=desc&limit=4
// by arrival = /products?sortBy=createdAt&order=desc&limit=4
// if no params are sent, then all products are returned
exports.list = (req, res) => {
  let order = req.query.order ? req.query.order : 'asc'
  let sortBy = req.query.sortBy ? req.query.sortBy : '_id'
  let limit = req.query.limit ? parseInt(req.query.limit) : parseInt(6)

  Product.find().select('-image').populate('category').sort([[sortBy, order]]).limit(limit).exec((err, products) => {
    if (err) return res.status(400).json({ err: 'Products not found' })
    res.json(products)
  })

}

/* Find the product based on the req product category */
// other products that has the same category will be returned
exports.listRelated = (req, res) => {
  let limit = req.query.limit ? parseInt(req.query.limit) : parseInt(6)

  Product.find({ _id: { $ne: req.product }, category: req.product.category }).limit(limit).populate('category', '_id name').exec((err, products) => {
    if (err) return res.status(400).json({ err: 'Products not found' })
    res.json(products)
  })
}

exports.listCategory = (req, res) => {
  Product.distinct('category', {}, (err, categories) => {
    if (err) return res.status(400).json({ err: 'Categories not found' })
    res.json(categories)
  })
}

/**
 * list products by search
 * we will implement product search in react frontend
 * we will show categories in checkbox and price range in radio buttons
 * as the user clicks on those checkbox and radio buttons
 * we will make api request and show the products to users based on what he wants
 */


exports.listBySearch = (req, res) => {
  let order = req.body.order ? req.body.order : "desc";
  let sortBy = req.body.sortBy ? req.body.sortBy : "_id";
  let limit = req.body.limit ? parseInt(req.body.limit) : 100;
  let skip = parseInt(req.body.skip);
  let findArgs = {};

  // console.log(order, sortBy, limit, skip, req.body.filters);
  // console.log("findArgs", findArgs);

  for (let key in req.body.filters) {
    if (req.body.filters[key].length > 0) {
      if (key === "price") {
        // gte -  greater than price [0-10]
        // lte - less than
        findArgs[key] = {
          $gte: req.body.filters[key][0],
          $lte: req.body.filters[key][1]
        };
      } else {
        findArgs[key] = req.body.filters[key];
      }
    }
  }

  Product.find(findArgs).select("-image").populate("category").sort([[sortBy, order]]).skip(skip).limit(limit).exec((err, data) => {
    if (err) return res.status(400).json({ err: 'Products not found' })
    res.json({ size: data.length, data });
  });
};

exports.image = (req, res, next) => {
  if (req.product.image.data) {
    res.set('Content-Type', req.product.image.contentType)
    return res.send(req.product.image.data)
  }
  next()
}