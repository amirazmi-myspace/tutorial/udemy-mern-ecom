const express = require('express')
const router = express.Router()

// import controllers
const { register, login, logout, requireSignin } = require('../controllers/auth')
const { userRegisterValidator } = require('../validator')

// routes
router.post('/register', userRegisterValidator, register)
router.post('/login', login)
router.get('/logout', logout)

router.get('/hello', requireSignin, (req, res) => {
  res.send('hello there')
})

module.exports = router